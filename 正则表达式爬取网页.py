# -*- coding: utf-8 -*-
import xlrd
import requests
import time

import xlwt
import re

def read_excel():
    # 文件位置
    ExcelFile = xlrd.open_workbook(u'E:/paqu/reptilian_01/MetaSploit.xlsx')
    sheet = ExcelFile.sheet_by_name('Sheet1')

    f = xlwt.Workbook()
    sheet1 = f.add_sheet('ide', cell_overwrite_ok=True)

    # 获取单元格内容
    for i in range(1,350):
        #获取网页内容
        rapid = sheet.cell(i, 1).value.encode('utf-8')
        url = rapid
        req = requests.get(url)
        page = req.text

        # 正则表达式获取<tr></tr>之间内容
        res_tr = r'<code>(.*?)</code>'
        m_list = re.findall(res_tr, page, re.S | re.M)
        #列表转字符串
        m_tr1="".join(m_list)

        #去除所有html标签
        dr = re.compile(r'<[^>]+>', re.S)
        m_tr2 = dr.sub('',m_tr1)
        #去除指定字符
        m_tr3 = re.sub('&lt;target-id&gt;','',m_tr2)
        m_tr4 = re.sub('&lt;action-name&gt;', '', m_tr3)
        print "第"+str(i)+"个网页爬取成功"

        if m_tr4!='':

            sheet1.write(i, 0, i)
            sheet1.write(i, 1, rapid)
            sheet1.write(i, 2, m_tr4)
            print m_tr4
    name=time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))
    f.save(name+".xlsx")
    print name+".xlsx"+"文件保存成功"

print read_excel()





